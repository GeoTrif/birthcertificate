package com.sda.geotrif.advance.model;

/*
 * BirthCertificate - nume, prenume, CNP, sex, loculNasterii
 */

public class BirthCertificate {

	private String nume;
	private String prenume;
	private String CNP;
	private char sex;
	private String loculNasterii;
	private int dataNasterii;
	private boolean bornInRomania;

	// Pentru a putea utiliza in mod corect si si constructori si setteri,vom face
	// un constructor default si vom face overloading la acest constructor.

	public BirthCertificate() {

	}

	public BirthCertificate(String nume, String prenume, String CNP, char sex, String loculNasterii, int dataNasterii,
			boolean bornInRomania) {
		this.nume = nume;
		this.prenume = prenume;
		this.CNP = CNP;
		this.sex = sex;
		this.loculNasterii = loculNasterii;
		this.dataNasterii = dataNasterii;
		this.bornInRomania = bornInRomania;
	}

	public void setNume(String nume) { // Pt a face o clasa imutabila,nu mai folosim setterele.
		this.nume = nume;
	}

	public String getNume() {
		return nume;
	}

	public void setPrenume(String prenume) {
		this.prenume = prenume;
	}

	public String getPrenume() {
		return prenume;
	}

	public void setCNP(String CNP) {
		this.CNP = CNP;
	}

	public String getCNP() {
		return CNP;
	}

	public void setSex(char sex) {
		this.sex = sex;
	}

	public char getSex() {
		return sex;
	}

	public void setLoculNasterii(String loculNasterii) {
		this.loculNasterii = loculNasterii;
	}

	public String getLoculNasterii() {
		return loculNasterii;
	}

	public void setDataNasterii(int dataNasterii) {
		this.dataNasterii = dataNasterii;
	}

	public int getDataNasterii() {
		return dataNasterii;
	}

	public void setBornInRomania(boolean bornInRomania) {
		this.bornInRomania = bornInRomania;
	}

	public boolean isBornInRomania() {
		return bornInRomania;
	}

	@Override // Facem overRide la metoda toString din clasa Object.
	public String toString() {
		return "BirthCertificate: " + " Name=" + this.nume + " Surmane=" + this.prenume + " CNP=" + this.CNP + " sex="
				+ this.sex + " birthLocation=" + this.loculNasterii + " birthYear=" + this.dataNasterii
				+ " bornInRomania=" + this.bornInRomania;
	}
}
