package com.sda.geotrif.advance.model;

import java.util.Scanner;

import org.omg.Messaging.SyncScopeHelper;

public class BirthCertificateTest {

	public static void main(String[] args) {

		Scanner input = new Scanner(System.in);

		BirthCertificate certificate = new BirthCertificate();

		System.out.print("Nume:");
		String nume = input.nextLine();
		certificate.setNume(nume);

		System.out.print("Prenume:");
		String prenume = input.nextLine();
		certificate.setPrenume(prenume);

		System.out.print("CNP:");
		String CNP = input.nextLine();
		certificate.setCNP(CNP);

		System.out.print("Sex:");
		char sex = input.nextLine().charAt(0);
		certificate.setSex(sex);

		System.out.print("Locul Nasterii:");
		String loculNasterii = input.nextLine();
		certificate.setLoculNasterii(loculNasterii);

		System.out.println("Data nasterii:");
		int dataNasterii = input.nextInt();
		certificate.setDataNasterii(dataNasterii);

		System.out.println("Esti nascut in Romania(Da/Nu):");
		String raspuns = input.next();
		certificate.setBornInRomania(true);

		if (raspuns.equals("Da")) {
			certificate.setBornInRomania(true);
		} else {
			certificate.setBornInRomania(false);
		}

		System.out.println("Nascut in Romania: ");
		boolean roman = input.nextBoolean();

		System.out.println("-------------------------------");

		System.out.println("Informatiile despre cetateanul " + certificate.getNume() + " " + certificate.getPrenume());
		System.out.println("Numele si prenumele:" + certificate.getNume() + " " + certificate.getPrenume());
		System.out.println("CNP:" + certificate.getCNP());
		System.out.println("Sex:" + certificate.getSex());
		System.out.println("Locul nasterii:" + certificate.getLoculNasterii());
		System.out.println("Data nasterii:" + certificate.getDataNasterii());
		System.out.println("Cetatean roman:" + certificate.isBornInRomania());

		System.out.println("$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$");

		BirthCertificate cert = new BirthCertificate(nume, prenume, CNP, sex, loculNasterii, dataNasterii, roman);

		System.out.println("Informatiile despre cetateanul " + cert.getNume() + " " + cert.getPrenume());
		System.out.println("Numele si prenumele:" + cert.getNume() + " " + cert.getPrenume());
		System.out.println("CNP:" + cert.getCNP());
		System.out.println("Sex:" + cert.getSex());
		System.out.println("Locul nasterii:" + cert.getLoculNasterii());
		System.out.println("Data nasterii:" + cert.getDataNasterii());
		System.out.println("Cetatean roman:" + cert.isBornInRomania());

		System.out.println(certificate.toString());
	}

}
